# Jr.-Data-Scientist-evaluation
Deployed URL: https://true-melons-dress-35-196-66-154.loca.lt



More Bonus points

Write about any difficult problem that you solved. (According to us difficult - is something which 90% of people would have only 10% probability in 
getting a similarly good solution). 
Sol: When i was making my project the movie recommender there was a lot of data manipulation in those two datasets that many times i used to forgot or got confused where 
i am in the project so i had to read and run the code from the beggining everytime and i was new to this so i had to do a lot of research from Youtube, Stackoverflow.



Explain back propagation and tell us how you handle a dataset if 4 out of 30 parameters have null values more than 40 percentage
Sol: Backpropagation is the essence of neural network training. It is the method of fine-tuning the weights of a neural network based on the error rate obtained in 
the previous epoch. Proper tuning of the weights allows you to reduce error rates and make the model reliable by increasing its generalization.
Backpropagation in neural network is a short form for “backward propagation of errors.” It is a standard method of training artificial neural networks. This method
helps calculate the gradient of a loss function with respect to all the weights in the network.

We can handle a dataset if 4 out of 30 parameters have null values more than 40 percentage by:
1. Impute missing values for continuous variables
2. Impute missing values for categorical variable
3. Deleting rows and columns
4. using Algorithms that support missing values

